# Graphical Human Interface

## Main Page/Login/Signup

Clinics are directed here first to login or singup

![Home Page](wireframes/login.signup.jpg)

## Appointments Page

Once a clinic has logged in they will be directed here and can view all of the appointments for the clinic and create appointments and register patients. They will also be able to toggle between upcoming and previous appointments.

![Appointment Page](wireframes/Appointments.jpg)

## Clinic Details

Here clinics will be able to view all of the providers and insurances at their clinic, and they will be able to add providers and insurances.

![ClinicDetails Page](wireframes/ClinicDetails.jpg)

## Account Details

On this page the clinic will be able to view there Clinic name, email, adress.

![Account Details Page](wireframes/ClinicAccount.jpg)
