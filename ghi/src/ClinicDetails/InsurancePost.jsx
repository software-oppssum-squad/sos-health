import { useState } from 'react'
import imgUrl from '../../public/assets/icon1.jpg'
const API_HOST = import.meta.env.VITE_API_HOST

const CreateInsurance = () => {
    const [formData, setFormData] = useState({
        clinic_uid: '',
        name: '',
    })
    const [showModal, setShowModal] = useState(false)

    const handleSubmit = async (event) => {
        event.preventDefault()

        // eslint-disable-next-line no-unused-vars
        const { clinic_uid, ...otherFields } = formData
        const isFormValid = Object.values(otherFields).every(
            (value) => value !== ''
        )

        if (!isFormValid) {
            console.error('Please fill out all fields')
            return
        }

        const url = `${API_HOST}/insurance`
        try {
            const response = await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(formData),
            })

            if (response.ok) {
                setFormData({
                    ...formData,
                    name: '',
                })
                setShowModal(true)
            } else {
                console.error('Failed to create Insurance.')
            }
        } catch (error) {
            console.error('Error creating Insurance:', error)
        }
    }

    const handleFormChange = (event) => {
        const { name, value } = event.target
        setFormData({
            ...formData,
            [name]: value,
        })
    }

    return (
        <section className="bg-gray-50 min-h-96 dark:bg-gray-900 p-3 sm:p-5">
            <div className="flex flex-col items-center px-6 py-8 mx-auto lg:py-0">
                <div
                    href="#"
                    className="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white"
                >
                    <img className="w-8 h-8 mr-2" src={imgUrl} alt="logo" />
                    SOS Healthcare Management
                </div>
                <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
                    <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
                        <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                            Add Insurance
                        </h1>
                        <form onSubmit={handleSubmit}>
                            <div className="mb-4">
                                <label
                                    htmlFor="name"
                                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                >
                                    Name
                                </label>
                                <input
                                    value={formData.name}
                                    onChange={handleFormChange}
                                    type="text"
                                    className="mt-1 p-2 block w-full border border-gray-300 rounded-md focus:outline-none focus:ring-blue-500 focus:border-blue-500"
                                    name="name"
                                />
                            </div>
                            <button
                                className="w-full bg-blue-500 text-white py-2 px-4 rounded-md hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2"
                                type="submit"
                            >
                                Create
                            </button>
                        </form>
                    </div>
                </div>
                {showModal ? (
                    <div
                        tabIndex="-1"
                        className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                    >
                        <div className="relative p-4 w-full max-w-md h-full md:h-auto">
                            <div className="relative p-4 text-center bg-white rounded-lg shadow dark:bg-gray-800 sm:p-5">
                                <div className="w-12 h-12 rounded-full bg-green-100 dark:bg-green-900 p-2 flex items-center justify-center mx-auto mb-3.5">
                                    <svg
                                        className="w-8 h-8 text-green-500 dark:text-green-400"
                                        fill="currentColor"
                                        viewBox="0 0 20 20"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path
                                            fillRule="evenodd"
                                            d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                            clipRule="evenodd"
                                        ></path>
                                    </svg>
                                    <span className="sr-only">Success</span>
                                </div>
                                <p className="mb-4 text-lg font-semibold text-gray-900 dark:text-white">
                                    Successfully Added An Insurance.
                                </p>
                                <button
                                    onClick={() => setShowModal(false)}
                                    type="button"
                                    className="py-2 px-3 text-sm font-medium text-center text-white rounded-lg bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 dark:focus:ring-primary-900"
                                >
                                    Continue
                                </button>
                            </div>
                        </div>
                    </div>
                ) : null}
            </div>
        </section>
    )
}

export default CreateInsurance
