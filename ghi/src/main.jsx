import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import { AuthProvider } from '@galvanize-inc/jwtdown-for-react'

// const basename = process.env.PUBLIC_URL
const baseUrl = import.meta.env.VITE_API_HOST
if (!baseUrl) {
    throw new Error('VITE_API_HOST is not defined')
}

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <AuthProvider baseUrl={baseUrl}>
            <App />
        </AuthProvider>
    </React.StrictMode>
)
