import { useEffect, useState } from 'react'
import imgUrl from '../../public/assets/icon1.jpg'

const API_HOST = import.meta.env.VITE_API_HOST

const CreateAppointment = function () {
    const [patients, setPatients] = useState([])
    const [patient, setPatient] = useState('')
    const [providers, setProviders] = useState([])
    const [provider, setProvider] = useState('')
    const [clinic, setClinic] = useState('')
    const [day, setDay] = useState('')
    const [type, setType] = useState('')
    const [showModal, setShowModal] = useState(false)

    const fetchData = async () => {
        const patientResponse = await fetch(`${API_HOST}/patient`, {
            credentials: 'include',
        })
        if (patientResponse.ok) {
            const data = await patientResponse.json()
            setPatients(data)
        }
        const providerResponse = await fetch(`${API_HOST}/provider`, {
            credentials: 'include',
        })
        if (providerResponse.ok) {
            const data = await providerResponse.json()
            setProviders(data)
        }
        const clinicResponse = await fetch(`${API_HOST}/clinictoken`, {
            credentials: 'include',
        })
        if (clinicResponse.ok) {
            const data = await clinicResponse.json()
            setClinic(data.clinic)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handlePatientChange = (event) => {
        const value = event.target.value
        setPatient(value)
    }

    const handleProviderChange = (event) => {
        const value = event.target.value
        setProvider(value)
    }

    const handleDayChange = (event) => {
        const value = event.target.value
        setDay(value)
    }

    const handleTypeChange = (event) => {
        const value = event.target.value
        setType(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.patient_uid = patient
        data.provider_uid = provider
        data.clinic_uid = clinic.uid
        data.day = day
        data.status = 'active'
        data.type = type
        const url = `${API_HOST}/appointment`
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const appointmentResponse = await fetch(url, fetchOptions)
        if (appointmentResponse.ok) {
            const newAppointment = await appointmentResponse.json()
            console.log('new appointment:', newAppointment)

            setPatient('')
            setProvider('')
            setClinic('')
            setDay('')
            setType('')
            setShowModal(true)
        }
    }

    return (
        <section className="bg-gray-50 min-h-96 dark:bg-gray-900 p-3 sm:p-5">
            <div className="flex flex-col items-center px-6 py-8 mx-auto lg:py-0">
                <div
                    href="#"
                    className="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white"
                >
                    <img className="w-8 h-8 mr-2" src={imgUrl} alt="logo" />
                    SOS Healthcare Management
                </div>
                <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
                    <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
                        <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                            make appointment
                        </h1>

                        <form className="max-w-lg mx-auto">
                            <div className="grid gap-6 mb-6 md:grid-cols-2">
                                <div>
                                    <label
                                        htmlFor="patient"
                                        className="sr-only"
                                    >
                                        Underline select
                                    </label>
                                    <select
                                        className="block py-2.5 px-0 w-full text-sm text-gray-500 bg-transparent border-0 border-b-2 border-gray-200 appearance-none dark:text-gray-400 dark:border-gray-700 focus:outline-none focus:ring-0 focus:border-gray-200 peer"
                                        onChange={handlePatientChange}
                                        id="patient"
                                    >
                                        <option value="">
                                            Select a patient
                                        </option>
                                        {patients.map((patient) => {
                                            return (
                                                <option
                                                    key={patient.uid}
                                                    value={patient.uid}
                                                >
                                                    {patient.f_name}{' '}
                                                    {patient.l_name}
                                                </option>
                                            )
                                        })}
                                    </select>
                                </div>
                                <div>
                                    <label
                                        htmlFor="patient"
                                        className="sr-only"
                                    >
                                        Underline select
                                    </label>
                                    <select
                                        className="block py-2.5 px-0 w-full text-sm text-gray-500 bg-transparent border-0 border-b-2 border-gray-200 appearance-none dark:text-gray-400 dark:border-gray-700 focus:outline-none focus:ring-0 focus:border-gray-200 peer"
                                        onChange={handleProviderChange}
                                        id="provider"
                                    >
                                        <option value="">
                                            Select a provider
                                        </option>
                                        {providers.map((provider) => {
                                            return (
                                                <option
                                                    key={provider.uid}
                                                    value={provider.uid}
                                                >
                                                    {provider.name}
                                                </option>
                                            )
                                        })}
                                    </select>
                                </div>
                            </div>
                            <div className="grid gap-6 mb-6 md:grid-cols-2">
                                <div>
                                    <label
                                        htmlFor="date"
                                        className="block mb-2 text sm font-medium text-gray-900 dark:text-white"
                                    >
                                        Date
                                    </label>
                                    <input
                                        className="shadow-lg block py-2.5 px-0 w-full text-sm text-gray-500 bg-transparent border-0 border-b-2 border-gray-200 appearance-none dark:text-gray-400 dark:border-gray-700 focus:outline-none focus:ring-0 focus:border-gray-200 peer"
                                        onChange={handleDayChange}
                                        placeholder="day"
                                        value={day}
                                        type="date"
                                        name="day"
                                        id="day"
                                    />
                                </div>
                                <div className="relative">
                                    <label
                                        htmlFor="type"
                                        className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto start-1"
                                    >
                                        Appointment Type
                                    </label>
                                    <input
                                        className="shadow-lg block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                        onChange={handleTypeChange}
                                        placeholder=" "
                                        value={type}
                                        type="text"
                                        name="type"
                                        id="type"
                                    />
                                </div>
                            </div>
                            <button
                                className="content-center shadow-lg text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
                                type="button"
                                onClick={handleSubmit}
                            >
                                Submit
                            </button>
                        </form>
                    </div>
                </div>
                {showModal ? (
                    <div
                        tabIndex="-1"
                        className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                    >
                        <div className="relative p-4 w-full max-w-md h-full md:h-auto">
                            <div className="relative p-4 text-center bg-white rounded-lg shadow dark:bg-gray-800 sm:p-5">
                                <div className="w-12 h-12 rounded-full bg-green-100 dark:bg-green-900 p-2 flex items-center justify-center mx-auto mb-3.5">
                                    <svg
                                        className="w-8 h-8 text-green-500 dark:text-green-400"
                                        fill="currentColor"
                                        viewBox="0 0 20 20"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path
                                            fillRule="evenodd"
                                            d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                            clipRule="evenodd"
                                        ></path>
                                    </svg>
                                    <span className="sr-only">Success</span>
                                </div>
                                <p className="mb-4 text-lg font-semibold text-gray-900 dark:text-white">
                                    Successfully Created an Appointment.
                                </p>
                                <button
                                    onClick={() => setShowModal(false)}
                                    type="button"
                                    className="py-2 px-3 text-sm font-medium text-center text-white rounded-lg bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 dark:focus:ring-primary-900"
                                >
                                    Continue
                                </button>
                            </div>
                        </div>
                    </div>
                ) : null}
            </div>
        </section>
    )
}

export default CreateAppointment
