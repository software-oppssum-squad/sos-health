import { useEffect, useState } from 'react'
import imgUrl from '../../public/assets/icon1.jpg'
const API_HOST = import.meta.env.VITE_API_HOST

const CreatePatient = function () {
    const [clinic, setClinic] = useState('')
    const [fName, setFName] = useState('')
    const [lName, setLName] = useState('')
    const [email, setEmail] = useState('')
    const [address, setAddress] = useState('')
    const [phone, setPhone] = useState('')
    const [dob, setDob] = useState('')
    const [pInitial, setPInitial] = useState('')
    const [showModal, setShowModal] = useState(false)

    const fetchData = async () => {
        const clinicResponse = await fetch(`${API_HOST}/clinictoken`, {
            credentials: 'include',
        })
        if (clinicResponse.ok) {
            const data = await clinicResponse.json()
            setClinic(data.clinic.uid)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleFNameChange = (event) => {
        const value = event.target.value
        setFName(value)
    }

    const handleLNameChange = (event) => {
        const value = event.target.value
        setLName(value)
    }

    const handleEmailChange = (event) => {
        const value = event.target.value
        setEmail(value)
    }

    const handleAddressChange = (event) => {
        const value = event.target.value
        setAddress(value)
    }

    const handlePhoneChange = (event) => {
        const value = event.target.value
        setPhone(value)
    }

    const handleDobChange = (event) => {
        const value = event.target.value
        setDob(value)
    }

    const handlePInitialChange = (event) => {
        const value = event.target.value
        setPInitial(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.account_uid = clinic
        data.f_name = fName
        data.l_name = lName
        data.email = email
        data.address = address
        data.phone = phone
        data.dob = dob
        data.p_initial = pInitial
        const url = `${API_HOST}/patient`
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchOptions)
        if (response.ok) {
            const newPatient = await response.json()
            console.log('new patient:', newPatient)

            setClinic('')
            setFName('')
            setLName('')
            setEmail('')
            setAddress('')
            setPhone('')
            setDob('')
            setPInitial('')
            setShowModal(true)
        }
    }

    return (
        <section className="bg-gray-50 min-h-96 dark:bg-gray-900 p-3 sm:p-5">
            <div className="flex flex-col items-center px-6 py-8 mx-auto lg:py-0">
                <div
                    href="#"
                    className="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white"
                >
                    <img className="w-8 h-8 mr-2" src={imgUrl} alt="logo" />
                    SOS Healthcare Management
                </div>
                <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
                    <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
                        <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                            Add a patient
                        </h1>
                        <form className="max-w-lg mx-auto">
                            <div className="grid gap-6 mb-4 md:grid-cols-2">
                                <div className="relative">
                                    <label
                                        htmlFor="f_name"
                                        className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto start-1"
                                    >
                                        First name
                                    </label>
                                    <input
                                        className="shadow-lg block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                        onChange={handleFNameChange}
                                        placeholder=" "
                                        value={fName}
                                        type="text"
                                        name="FName"
                                        id="FName"
                                    />
                                </div>
                                <div className="relative">
                                    <label
                                        htmlFor="l_name"
                                        className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto start-1"
                                    >
                                        Last name
                                    </label>
                                    <input
                                        className="shadow-lg block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                        onChange={handleLNameChange}
                                        placeholder=""
                                        value={lName}
                                        type="text"
                                        name="LName"
                                        id="LName"
                                    />
                                </div>
                            </div>
                            <div className="mb-4 relative">
                                <label
                                    htmlFor="email"
                                    className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto start-1"
                                >
                                    Email
                                </label>
                                <input
                                    className="shadow-lg block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                    onChange={handleEmailChange}
                                    placeholder=""
                                    value={email}
                                    type="text"
                                    name="email"
                                    id="email"
                                />
                            </div>
                            <div className="mb-4 relative">
                                <label
                                    htmlFor="address"
                                    className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto start-1"
                                >
                                    Address
                                </label>
                                <input
                                    className="shadow-lg block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                    onChange={handleAddressChange}
                                    placeholder=""
                                    value={address}
                                    type="text"
                                    name="address"
                                    id="address"
                                />
                            </div>
                            <div className="grid gap-6 mb-6 md:grid-cols-3">
                                <div className="flex items-center">
                                    <div className="relative">
                                        <label
                                            htmlFor="phone"
                                            className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto start-1"
                                        >
                                            Phone
                                        </label>
                                        <input
                                            className="shadow-lg block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                            onChange={handlePhoneChange}
                                            placeholder=""
                                            value={phone}
                                            type="text"
                                            name="phone"
                                            id="phone"
                                        />
                                    </div>
                                </div>
                                <div className="relative">
                                    <label
                                        htmlFor="dob"
                                        className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto start-1"
                                    >
                                        Date of Birth
                                    </label>
                                    <input
                                        className="shadow-lg block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                        onChange={handleDobChange}
                                        placeholder=""
                                        value={dob}
                                        type="date"
                                        name="dob"
                                        id="dob"
                                    />
                                </div>
                                <div className="relative">
                                    <label
                                        htmlFor="p_initial"
                                        className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto start-1"
                                    >
                                        Initials
                                    </label>
                                    <input
                                        className="shadow-lg block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                                        onChange={handlePInitialChange}
                                        placeholder=""
                                        value={pInitial}
                                        type="text"
                                        name="pInitial"
                                        id="pInital"
                                    />
                                </div>
                            </div>
                            <button
                                className="content-center shadow-lg text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
                                onClick={handleSubmit}
                                type="button"
                            >
                                Submit
                            </button>
                        </form>
                    </div>
                </div>
                {showModal ? (
                    <div
                        tabIndex="-1"
                        className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                    >
                        <div className="relative p-4 w-full max-w-md h-full md:h-auto">
                            <div className="relative p-4 text-center bg-white rounded-lg shadow dark:bg-gray-800 sm:p-5">
                                <div className="w-12 h-12 rounded-full bg-green-100 dark:bg-green-900 p-2 flex items-center justify-center mx-auto mb-3.5">
                                    <svg
                                        className="w-8 h-8 text-green-500 dark:text-green-400"
                                        fill="currentColor"
                                        viewBox="0 0 20 20"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path
                                            fillRule="evenodd"
                                            d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                            clipRule="evenodd"
                                        ></path>
                                    </svg>
                                    <span className="sr-only">Success</span>
                                </div>
                                <p className="mb-4 text-lg font-semibold text-gray-900 dark:text-white">
                                    Successfully Added a Patient.
                                </p>
                                <button
                                    onClick={() => setShowModal(false)}
                                    type="button"
                                    className="py-2 px-3 text-sm font-medium text-center text-white rounded-lg bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 dark:focus:ring-primary-900"
                                >
                                    Continue
                                </button>
                            </div>
                        </div>
                    </div>
                ) : null}
            </div>
        </section>
    )
}

export default CreatePatient
