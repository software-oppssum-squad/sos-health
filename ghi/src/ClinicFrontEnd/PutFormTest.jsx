// THIS DOES NOT WORK YET
// THIS DOES NOT WORK YET
// THIS DOES NOT WORK YET

import { useEffect, useState } from 'react'

const API_HOST = import.meta.env.VITE_API_HOST

const UpdateClinic = function () {
    // eslint-disable-next-line no-unused-vars
    const [clinic, setClinic] = useState('')
    const [clinicId, setClinicId] = useState('')
    const [formData, setFormData] = useState({
        name: '',
        email: '',
        address: '',
        photo: 'Under Construction',
    })

    const fetchClinicToken = async () => {
        const clinicResponse = await fetch(
            `${API_HOST}/clinictoken`,
            {
                credentials: 'include',
            }
        )
        if (clinicResponse.ok) {
            const data = await clinicResponse.json()
            console.log('clinic data', data.clinic)
            setClinic(data.clinic)
            setFormData({
                ...formData,
                name: data.clinic.name,
                email: data.clinic.email,
                address: data.clinic.address,
                photo: data.clinic.photo,
            })
            setClinicId(data.clinic.id)
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name
        setFormData({
            ...formData,
            [inputName]: value,
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        console.log('!!!!!!!clinicId', clinicId)

        const url = `${API_HOST}/clinic/${clinicId}`
        const fetchConfig = {
            method: 'put',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(url, fetchConfig)

        if (response.ok) {
            setFormData({
                name: '',
                email: '',
                address: '',
                photo: 'Under Construction',
            })
            console.log('!!!!!!!!response', response)
        }
    }

    useEffect(() => {
        fetchClinicToken()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <div className="offset-3 col-6">
            <h1>Update Clinic Detail</h1>
            <form onSubmit={handleSubmit} id="update-clinic-form">
                <div className="form-floating mb-3">
                    <input
                        value={formData.name}
                        placeholder="name"
                        onChange={handleFormChange}
                        type="text"
                        name="name"
                        className="form-control"
                    />
                    <label htmlFor="name">name</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                        value={formData.email}
                        placeholder="email"
                        onChange={handleFormChange}
                        type="email"
                        name="email"
                        className="form-control"
                    />
                    <label htmlFor="email">email</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                        value={formData.address}
                        onChange={handleFormChange}
                        placeholder="address"
                        required
                        type="text"
                        name="address"
                        className="form-control"
                    />
                    <label htmlFor="address">Reason</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                        value={formData.photo}
                        onChange={handleFormChange}
                        placeholder="photo"
                        required
                        type="text"
                        name="photo"
                        className="form-control"
                    />
                    <label htmlFor="photo">photo</label>
                </div>
                <button className="btn btn-primary w-100">Update</button>
            </form>
        </div>
    )
}

export default UpdateClinic
