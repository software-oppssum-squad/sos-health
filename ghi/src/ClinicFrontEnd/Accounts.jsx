import { useEffect, useState } from 'react'

const API_HOST = import.meta.env.VITE_API_HOST

function Accounts() {
    const [clinicaccount, setCAccount] = useState([])
    const [showModal, setShowModal] = useState(false)
    const [Data, setData] = useState({
        name: '',
        email: '',
        address: '',
        photo: 'Under Construction',
    })

    const getAccounts = async () => {
        const accountUrl = `${API_HOST}/clinictoken`
        const response = await fetch(accountUrl, {
            credentials: 'include',
        })
        console.log(accountUrl)
        if (response.ok) {
            const data = await response.json()
            setCAccount(data.clinic)
        }
    }

    useEffect(() => {
        getAccounts()
    }, [])

    const handleChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name
        setData({
            ...Data,
            [inputName]: value,
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const account = `${API_HOST}/clinic/${clinicaccount.id}`
        const fetchConfig = {
            method: 'put',
            body: JSON.stringify(Data),
            headers: {
                'Content-Type': 'application/json',
            },
            credentials: 'include',
        }
        console.log('yeet', fetchConfig)
        console.log('tokencheck', Data)
        const response = await fetch(account, fetchConfig)
        if (response.ok) {
            setData({
                name: '',
                email: '',
                address: '',
                photo: 'Under Construction',
            })
            setShowModal(true)
        }
    }

    // {credentials: 'include',}

    return (
        <section className="bg-gray-50 min-h-96 dark:bg-gray-900 p-3 sm:p-5">
            <div className="mx-auto max-w-screen-xl px-4 lg:px-12">
                <div className="text-3xl font-extrabold leading-tight tracking-tight text-gray-900 sm:text-4xl dark:text-white">
                    Clinic Account Details
                </div>
                <div className="flex flex-col md:flex-row items-center justify-between space-y-3 md:space-y-0 md:space-x-4 p-4">
                    <div className="w-full md:w-1/2">
                        <form className="space-y-4 md:space-y-6">
                            <div>
                                <label
                                    htmlFor="name"
                                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                >
                                    Clinic Name
                                </label>
                                <input
                                    onChange={handleChange}
                                    value={Data.name}
                                    placeholder={clinicaccount.name}
                                    required
                                    type="text"
                                    name="name"
                                    className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                />
                            </div>
                            <div>
                                <label
                                    htmlFor="email"
                                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                >
                                    Email
                                </label>
                                <input
                                    onChange={handleChange}
                                    value={Data.email}
                                    placeholder={clinicaccount.email}
                                    required
                                    type="email"
                                    name="email"
                                    className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                />
                            </div>
                            <div>
                                <label
                                    htmlFor="address"
                                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                >
                                    Mailing Address
                                </label>
                                <input
                                    onChange={handleChange}
                                    value={Data.address}
                                    placeholder={clinicaccount.address}
                                    required
                                    type="text"
                                    name="address"
                                    className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                />
                            </div>
                            <div>
                                <label
                                    htmlFor="photo"
                                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                >
                                    Photo
                                </label>
                                <input
                                    onChange={handleChange}
                                    value={Data.photo}
                                    placeholder={clinicaccount.photo}
                                    required
                                    type="text"
                                    name="photo"
                                    className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                />
                            </div>
                            <div>
                                <button
                                    type="button"
                                    onClick={handleSubmit}
                                    className="content-center shadow-lg text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
                                >
                                    Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                {showModal ? (
                    <div
                        tabIndex="-1"
                        className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                    >
                        <div className="relative p-4 w-full max-w-md h-full md:h-auto">
                            <div className="relative p-4 text-center bg-white rounded-lg shadow dark:bg-gray-800 sm:p-5">
                                <div className="w-12 h-12 rounded-full bg-green-100 dark:bg-green-900 p-2 flex items-center justify-center mx-auto mb-3.5">
                                    <svg
                                        className="w-8 h-8 text-green-500 dark:text-green-400"
                                        fill="currentColor"
                                        viewBox="0 0 20 20"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path
                                            fillRule="evenodd"
                                            d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                            clipRule="evenodd"
                                        ></path>
                                    </svg>
                                    <span className="sr-only">Success</span>
                                </div>
                                <p className="mb-4 text-lg font-semibold text-gray-900 dark:text-white">
                                    Successfully Updated Account Information.
                                </p>
                                <button
                                    onClick={() => setShowModal(false)}
                                    type="button"
                                    className="py-2 px-3 text-sm font-medium text-center text-white rounded-lg bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 dark:focus:ring-primary-900"
                                >
                                    Continue
                                </button>
                            </div>
                        </div>
                    </div>
                ) : null}
            </div>
        </section>
    )
}

export default Accounts
