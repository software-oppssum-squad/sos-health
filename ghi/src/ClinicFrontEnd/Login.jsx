import { useState, useEffect } from 'react'
import useToken from '@galvanize-inc/jwtdown-for-react'
import { useNavigate } from 'react-router-dom'
import { NavLink } from 'react-router-dom'
import imgUrl from '../../public/assets/image1.jpg'

// we might be able to remove props as a param here
// const api host here
const API_HOST = import.meta.env.VITE_API_HOST

// eslint-disable-next-line no-unused-vars
function Login(props) {
    const { login } = useToken()
    const nav = useNavigate()

    const [loginData, setLogInData] = useState({
        username: '',
        password: '',
    })
    const checkForToken = async () => {
        const clinicResponse = await fetch(`${API_HOST}/clinictoken`, {
            credentials: 'include',
        })
        if (clinicResponse.ok) {
            const data = await clinicResponse.json()
            if (data) {
                nav('/appointments')
            }
        }
    }

    useEffect(() => {
        checkForToken()
    })

    const handleLogin = async (e) => {
        e.preventDefault()
        // eslint-disable-next-line no-unused-vars
        const logging = await login(loginData.username, loginData.password)
        const clinicResponse = await fetch(
            // use environ
            `${API_HOST}/clinictoken`,
            {
                credentials: 'include',
            }
        )
        if (clinicResponse.ok) {
            const data = await clinicResponse.json()
            if (data) {
                nav('/appointments')
                setLogInData({
                    username: '',
                    password: '',
                })
            }
        }
    }

    const handleLoginChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name
        setLogInData({
            ...loginData,
            [inputName]: value,
        })
    }

    return (
        <div className="grid grid-cols-12">
            <div className="col-span-4 text-white font-sans font-bold bg-black min-h-screen pl-7">
                <div className="grid grid-rows-6 grid-flow-col min-h-screen items-center justify-items-start mx-20">
                    <div className="row-span-4 row-start-2 text-4xl">
                        Welcome To SOS Health!
                        <form>
                            <label className="text-sm font-sans font-medium">
                                Username
                            </label>
                            <input
                                value={loginData.username}
                                onChange={handleLoginChange}
                                type="text"
                                name="username"
                                placeholder="username"
                                className="w-full bg-black py-3 px-12 border hover: border-gray-500 rounded shadow text-base font-sans"
                            />

                            <label className="text-sm font-sans font-medium">
                                Password
                            </label>
                            <input
                                value={loginData.password}
                                onChange={handleLoginChange}
                                type="password"
                                name="password"
                                placeholder="Password"
                                className=" w-full bg-black py-3 px-12 border hover: border-gray-500 rounded shadow text-base font-sans"
                            />
                            <div
                                href=""
                                className="text-sm font-sans font-medium text-gray-600 underline"
                            >
                                Forgot password?
                            </div>
                            {/* <!-- Button --> */}
                            <div className="text-sm font-sans font-medium w-full pr-20 pt-14">
                                <button
                                    onClick={handleLogin}
                                    type="button"
                                    className="text-center w-full py-4 bg-blue-700 hover:bg-blue-400 rounded-md text-white"
                                >
                                    SIGN IN
                                </button>
                            </div>
                        </form>
                    </div>
                    <NavLink aria-current="page" to="/signup">
                        <div
                            href=""
                            className="text-sm font-sans font-medium text-gray-400 underline"
                        >
                            <button className="block text-white bg-primary-700 hover:bg-primary-800 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">
                                Don´t have an account? Sign up
                            </button>
                        </div>
                    </NavLink>
                    {/* <!-- Text --> */}
                </div>
            </div>

            {/* <!-- Second column image --> */}
            <div className="pr-banner col-span-8 text-white font-sans font-bold"></div>
            <style>
                {`.pr-banner {
        background:url(${imgUrl});
        background-repeat: no-repeat;
        background-size: cover
    }`}
            </style>
        </div>
    )
}

export default Login
