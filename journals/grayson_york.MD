## Week 17
  Week 17 is just finishing up the project and deployment. I started the week putting tailwind into my pages, I do not like working with tailwind. Next I worked on deployment, which took me a couple days. I'm still running into some issues where i can navigate to the site root, and through the site through there, but refreshing pages or navigating to the site not starting at the root causes a 404, 401, or 503, in what feels like no discernable pattern. Rosheen said she would like to help us next week after grading, which is just fine with me. I am finishing up cleaning up dead code, hopefully nothing breaks!

## Week 16
  I worked on mostly frontend this week. I made 3 pages for creating patients, appointmnets and listing appointments. they look bad. I also changed the backend to generate uids and changed all queries to start using uid instead of id. its a pain, and doesnt practically change any function of the app, but the practice is always good, i guess. I also made a couple of unit tests for insurance.


## Week 15
  This was the last major backend week. I completed the auth for accounts, but we ended up changing from accounts to clinic, so Philip had to write it again. Hopefully my code helped. I finished all my appointment endpoints, and they work. Hooray!


## Week 14

### February 28, 2024

I presented in the standup today

Today, I worked on:

* getting a sample endpoint functioning

### February 27, 2024

Today, I worked on:

* gettting the database set up
* writing a couple test tables



## Week 13
Week 13 was soley design. We decided to create a healthcare app centered around making and showing appointments. We had some concerns with using dateTime, but hopefully the learning experience will match the level of pain we subscribed ourselves to. We are just starting with day appointments which should make handling datetime a lot easier, but I really hope we are able to progress into hour slots.

Everything must be aligned. Dissent is unnacceptable.



Remember:
    Any classes you write that talk to postgres will need to use the DATABASE_URL environment variable to create a connection.

    <<# ./api/queries/accounts.py
    import os
    from psycopg_pool import ConnectionPool
    pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))>>


## July 9, 2021

Today, I worked on:

* Getting a customer's data for the account page
  with Asa

Asa and I wrote some SQL. We tested it out with
Insomnia and the UI. We had to coordinate with
Petra, who was working on the GHI with Azami.

Today, I found the F2/Rename symbol functionality
in Visual Studio Code! It allows me to rename a
variable without causing bugs. I am going to be
using this a lot!

## July 7, 2021

Today, I worked on:

* Signing up a customer with Petra

Petra and I had a hard time figuring out the logic
for what happens when a customer signs up. We
finally came to the conclusion that not only did we
have to create the `User` record, but we also needed
to create associated preference records **and** had
to log them in.

Today, database transactions finally clicked. It
happened while we were trying to insert the
preferences for the customer. We had a bug in the
code, so the `User` record got created, but none
of the preferences. All of the inserts should
succeed or fail together. So, we used a transaction
to do that.
