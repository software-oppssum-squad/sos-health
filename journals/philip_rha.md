** 3/19/2024 **

-   Implementing app wide CSS editing and modals.
-   Implementing Logout and rerouting after submissions.

** 3/12/2024 **

-   Working on Frontend for Clinic with tailwind

** 3/7/2024 **

-   Implemented UUID to all endpoints.

** 3/6/2024 **

-   After group discussion, we decided to rework how clinic endpoint will be operating. Clinic has been merged with account and will be used as USER endpoint.

** 3/4/2024 **

-   Finished endpoints for Clinic

** 2/26/2024**
&&&&&&&Today we are finalizing the project idea documentations&&&&&&&

-   Current focus is to familiarize with gitlab workflow and project settings.
-   We also need to have discussion on workload distribution.
