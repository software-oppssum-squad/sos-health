from fastapi import FastAPI  # APIRouter
from fastapi.middleware.cors import CORSMiddleware
from routers import (
    insurance,
    clinic,
    appointments,
    patients,
    provider
)
from authenticator import authenticator
import os


app = FastAPI()
app.include_router(authenticator.router)
app.include_router(appointments.router)
app.include_router(clinic.router)
app.include_router(insurance.router)
app.include_router(patients.router)
app.include_router(provider.router)


"""
in any endpoint you want to secure to a logged in user
use authenticator.get_Current_account_data method

account_data: dict = Depends(authenticator.get_Current_account_data)
"""


app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:5173")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00"
        }
    }
