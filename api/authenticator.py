import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.clinic import ClinicRepository, PrivateAccount


class MyAuthenticator(Authenticator):
    async def get_account_data(
        self,
        username: str,
        clinics: ClinicRepository,
    ):
        return clinics.get(username)

    def get_account_getter(
        self,
        clinics: ClinicRepository = Depends(),
    ):
        return clinics

    def get_hashed_password(self, clinic: PrivateAccount):
        return clinic.hashed_password

    def get_account_data_for_cookie(self, clinic: PrivateAccount):
        return clinic.username, PrivateAccount(**clinic.dict())


authenticator = MyAuthenticator(os.environ["SIGNING_KEY"])
