from fastapi.testclient import TestClient
from main import app
from queries.provider import ProviderRepository


client = TestClient(app)


class EmptyProvider:
    def get_all(self):
        return []


class CreateProvider:
    def create(self, provider):
        result = {
            "id": 1,
            "uid": "27f117ab-44ec-4378-bdca-d526c311e540",
            "name": "Nick",
            "email": "string@gmail.com",
            "specialty": "Surgery",
            "phone": "2124567890",
        }
        result.update(provider)
        return result


def test_get_all_providers():
    app.dependency_overrides[ProviderRepository] = EmptyProvider
    response = client.get("/provider")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == []


def test_create_provider():
    app.dependency_overrides[ProviderRepository] = CreateProvider

    json = {
        "name": "Nick",
        "email": "string@gmail.com",
        "specialty": "Surgery",
        "phone": "2124567890",
    }

    expected = {
        "id": 1,
        "uid": "27f117ab-44ec-4378-bdca-d526c311e540",
        "name": "Nick",
        "email": "string@gmail.com",
        "specialty": "Surgery",
        "phone": "2124567890",
    }

    response = client.post("/provider", json=json)

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected
