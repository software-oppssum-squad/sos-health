from fastapi.testclient import TestClient
from main import app
from queries.appointments import AppointmentRepository

client = TestClient(app)


class EmptyAppointment:
    def get_all(self):
        return []


class CreateAppointment:
    def create(self, appointment):
        result = {
            'id': 1,
            'uid': "testtestuuid",
            'patient_uid': 'testtestuuid',
            'provider_uid': 'testtestuuid',
            'clinic_uid': 'testtestuuid',
            "day": "2024-03-20",
            'status': 'test',
            'type': 'test'
            }
        result.update(appointment)
        return result


def test_get_all_appointment():
    app.dependency_overrides[AppointmentRepository] = EmptyAppointment
    response = client.get("/appointment")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == []


def test_create_appointment():
    app.dependency_overrides[AppointmentRepository] = CreateAppointment

    json = {
            'patient_uid': 'testtestuuid',
            'provider_uid': 'testtestuuid',
            'clinic_uid': 'testtestuuid',
            "day": "2024-03-20",
            'status': 'test',
            'type': 'test'
    }

    expected = {
            'id': 1,
            'uid': "testtestuuid",
            'patient_uid': 'testtestuuid',
            'provider_uid': 'testtestuuid',
            'clinic_uid': 'testtestuuid',
            "day": "2024-03-20",
            'status': 'test',
            'type': 'test'
    }

    response = client.post("/appointment", json=json)

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected
