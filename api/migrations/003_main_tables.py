# uid VARCHAR(36) DEFAULT gen_random_uuid()
steps = [
    [
        """
        CREATE TABLE clinic (
            id SERIAL PRIMARY KEY NOT NULL,
            uid VARCHAR(36) DEFAULT gen_random_uuid(),
            name VARCHAR(100) NOT NULL,
            username VARCHAR(50) UNIQUE NOT NULL,
            email VARCHAR(50) UNIQUE NOT NULL,
            hashed_password VARCHAR(500),
            address VARCHAR(100),
            photo VARCHAR(200)
        );
        """,
        """
        DROP TABLE clinic;
        """
    ],
    [
        """
        CREATE TABLE insurance (
            id SERIAL PRIMARY KEY NOT NULL,
            uid VARCHAR(36) default gen_random_uuid(),
            name VARCHAR(150) NOT NULL,
            clinic_uid VARCHAR(50) NOT NULL
        );
        """,
        """
        DROP TABLE insurance;
        """
    ],
    [
        """
        CREATE TABLE appointment(
            id SERIAL PRIMARY KEY NOT NULL,
            uid VARCHAR(36) default gen_random_uuid(),
            patient_uid VARCHAR(50) NOT NULL,
            provider_uid VARCHAR(50) NOT NULL,
            clinic_uid VARCHAR(50) NOT NULL,
            day DATE NOT NULL,
            status VARCHAR(50) NOT NULL,
            type VARCHAR(50)
        );
        """,
        """
        DROP TABLE appointment;
        """
    ],
    [
        """
        CREATE TABLE patient(
            id SERIAL PRIMARY KEY NOT NULL,
            uid VARCHAR(36) default gen_random_uuid(),
            account_uid VARCHAR(50),
            f_name VARCHAR(50) NOT NULL,
            l_name VARCHAR(50) NOT NULL,
            email VARCHAR(50) NOT NULL,
            address VARCHAR(100) NOT NULL,
            phone VARCHAR(15) NOT NULL,
            dob DATE NOT NULL,
            p_initial VARCHAR(15) NOT NULL
        );
        """,
        """
        DROP TABLE patient;
        """
    ],
    [
        """
        CREATE TABLE account(
            id SERIAL PRIMARY KEY NOT NULL,
            uid VARCHAR(36) default gen_random_uuid(),
            username VARCHAR(50) NOT NULL,
            password VARCHAR(50) NOT NULL,
            email VARCHAR(50) NOT NULL,
            hashed_password VARCHAR(500)
        );
        """,
        """
        DROP TABLE account;
        """
    ],
    [
        """
        CREATE TABLE provider(
            id SERIAL PRIMARY KEY NOT NULL,
            uid VARCHAR(36) default gen_random_uuid(),
            name VARCHAR(50) NOT NULL,
            email VARCHAR(50) NOT NULL,
            specialty VARCHAR(50) NOT NULL,
            phone VARCHAR(15) NOT NULL
        );
        """,
        """
        DROP TABLE provider;
        """
    ]
]
