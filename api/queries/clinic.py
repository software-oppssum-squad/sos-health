from pydantic import BaseModel
from queries.pool import pool
from typing import List, Optional, Union


class DuplicateClinicError(ValueError):
    pass


class Error(BaseModel):
    message: str


class ClinicIn(BaseModel):
    name: str
    username: str
    password: str
    email: str
    address: str
    photo: str


class ClinicOut(BaseModel):
    id: str
    uid: str
    name: str
    username: str
    email: str
    address: str
    photo: str


class PrivateAccount(ClinicOut):
    hashed_password: str


class PutPasswordIn(BaseModel):
    password: str


class PutPasswordOut(BaseModel):
    hashed_password: str


class PutClinicOut(BaseModel):
    name: str
    email: str
    address: str
    photo: str


class PutClinicIn(BaseModel):
    name: str
    email: str
    address: str
    photo: str


class ClinicRepository:
    def get(self, username: str) -> PrivateAccount:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                            SELECT *
                            FROM clinic
                            WHERE username = %s
                        """,
                        [username]
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return PrivateAccount(
                        id=record[0],
                        uid=record[1],
                        # uid=str(record[1])
                        name=record[2],
                        username=record[3],
                        email=record[4],
                        hashed_password=record[5],
                        address=record[6],
                        photo=record[7]
                    )
        except Exception as e:
            print("error", e, "has occurred :(")

    def get_all(self) -> List[ClinicOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, uid, name, username, email,
                        hashed_password, address, photo
                        FROM clinic
                        ORDER BY id;
                        """
                    )
                    clinics = []
                    for record in db:
                        clinic = ClinicOut(
                            id=record[0],
                            uid=record[1],
                            name=record[2],
                            username=record[3],
                            email=record[4],
                            address=record[5],
                            photo=record[6]
                        )
                        clinics.append(clinic)
                    return clinics
        except Exception as e:
            print("error", e, "has occurred :(")

    def create(self, clinic: ClinicIn, hashed_password: str) -> PrivateAccount:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO clinic
                        (name, username, email,
                        hashed_password, address, photo)
                        VALUES
                        (%s, %s, %s, %s, %s, %s)
                        RETURNING *;
                        """,
                        [clinic.name, clinic.username,
                            clinic.email, hashed_password,
                            clinic.address, clinic.photo],
                    )
                    record = result.fetchone()
                    print("!!!!!!!!!!!!!!uid!!!!!", record)
                    return PrivateAccount(
                            id=record[0],
                            uid=record[1],
                            name=record[2],
                            username=record[3],
                            email=record[4],
                            hashed_password=record[5],
                            address=record[6],
                            photo=record[7]
                    )
        except Exception as e:
            print("error", e, "has occurred :(")

    def delete(self, id: int) -> bool:
        try:
            print("trying")
            with pool.connection() as conn:
                with conn.cursor() as db:
                    print(id)
                    db.execute(
                        """
                        DELETE FROM clinic
                        WHERE id = %s
                        """,
                        [id],
                    )
                    print(db, "db")
                    return True
        except Exception as e:
            print("Failed", e)
            return False

    def get_one(self, id: int) -> Optional[ClinicOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, uid, name, username, email,
                        address, photo
                        FROM clinic
                        WHERE id = %s
                        """,
                        [id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return ClinicOut(
                        id=record[0],
                        uid=record[1],
                        name=record[2],
                        username=record[3],
                        email=record[4],
                        address=record[5],
                        photo=record[6]
                    )
        except Exception as e:
            print("Failed", e)

    def update(self, id: int, clinic: PutClinicIn
               ) -> Union[Error, PutClinicOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE clinic
                        SET name = %s, email = %s, address = %s, photo = %s
                        WHERE id = %s
                        """,
                        [clinic.name,
                         clinic.email,
                         clinic.address,
                         clinic.photo,
                         id],
                    )
                    old_data = clinic.dict()
                    return PutClinicOut(id=id, **old_data)
        except Exception as e:
            print("Failed", e)

    def update_password(self, id: int,
                        hashed_password: str) -> PutPasswordOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE clinic
                        SET hashed_password = %s
                        WHERE id = %s
                        """,
                        [hashed_password, id],
                    )
                    return PutPasswordOut(hashed_password=hashed_password)
        except Exception as e:
            print("Failed", e)
