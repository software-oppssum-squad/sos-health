from pydantic import BaseModel
from queries.pool import pool


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    uid: str
    username: str
    password: str
    email: str


class AccountOut(BaseModel):
    id: str
    uid: str
    username: str
    password: str
    email: str


class PrivateAccount(AccountOut):
    hashed_password: str


class AccountRepository:

    def get(self, username: str) -> PrivateAccount:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        SELECT *
                        FROM account
                        WHERE username = %s
                    """,
                    [username]
                )
                record = result.fetchone()
                if record is None:
                    return None
                return PrivateAccount(
                    id=record[0],
                    uid=record[1],
                    username=record[2],
                    password=record[3],
                    email=record[4],
                    hashed_password=record[5]
                )

    def create(self, info: AccountIn, hashed_password: str) -> PrivateAccount:
        print("we made it into the create method!!!!!!!!!!")
        with pool.connection() as conn:
            print("we made it through the pool connection")
            with conn.cursor() as db:
                print("we made it into the cursor")
                result = db.execute(
                    """
                        INSERT INTO account
                        (uid, username, password, email, hashed_password)
                        VALUES
                        (%s, %s, %s, %s, %s)
                        RETURNING *;
                    """,
                    [info.uid,
                     info.username,
                     info.password,
                     info.email,
                     hashed_password
                     ]
                )

                record = result.fetchone()
                print("here is the record:", record)
                return PrivateAccount(
                    id=record[0],
                    uid=record[1],
                    username=record[2],
                    password=record[3],
                    email=record[4],
                    hashed_password=record[5]
                )

    #         id SERIAL PRIMARY KEY NOT NULL,
    #         uid VARCHAR(50) UNIQUE NOT NULL,
    #         username VARCHAR(50) NOT NULL,
    #         password VARCHAR(50) NOT NULL,
    #         email VARCHAR(50) NOT NULL,
    #         hashed_password VARCHAR(50)
