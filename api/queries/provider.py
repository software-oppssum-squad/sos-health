from pydantic import BaseModel
from typing import Dict, List, Optional, Union
from queries.pool import pool


class Error(BaseModel):
    message: str


class ProviderIn(BaseModel):
    name: str
    email: str
    specialty: str
    phone: str


class ProviderOut(BaseModel):
    id: int
    uid: str
    name: str
    email: str
    specialty: str
    phone: str


class PutProviderIn(BaseModel):
    name: str
    email: str
    specialty: str
    phone: str


class PutProviderOut(BaseModel):
    name: str
    email: str
    specialty: str
    phone: str


class ProviderRepository:

    def get_one(self, uid: str) -> Optional[ProviderOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM provider
                        WHERE uid = %s
                        """,
                        [uid],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return ProviderOut(
                        id=record[0],
                        uid=record[1],
                        name=record[2],
                        email=record[3],
                        specialty=record[4],
                        phone=record[5],
                    )
        except Exception as T:
            print("T", T)
            return {"message": "Could not get provider"}

    def get_all(self) -> List[ProviderOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, uid, name, email, specialty, phone
                        FROM provider
                        ORDER BY name;
                        """
                    )
                    providers = []
                    for record in db.fetchall():
                        provider = ProviderOut(
                            id=record[0],
                            uid=record[1],
                            name=record[2],
                            email=record[3],
                            specialty=record[4],
                            phone=record[5],
                        )
                        providers.append(provider)
                    return providers
        except Exception as T:
            print("T", T)
            return []

    def create(self, provider: ProviderIn) -> Union[ProviderOut, Dict
                                                    [str, str]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO provider
                        (name, email, specialty, phone)
                        VALUES
                        (%s, %s, %s, %s)
                        RETURNING *;
                        """,
                        [provider.name, provider.email, provider.specialty,
                         provider.phone]
                    )
                    record = db.fetchone()
                    if record:
                        return ProviderOut(
                            id=record[0],
                            uid=record[1],
                            name=record[2],
                            email=record[3],
                            specialty=record[4],
                            phone=record[5]
                        )
                    else:
                        return {"message": "Inserted record not found"}
        except Exception as T:
            print("T", T)
            return {"message": "Could not create provider"}

    def update(self, id: int, provider: PutProviderIn) -> Union[Error,
                                                                PutProviderOut
                                                                ]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE provider
                        SET name = %s, email = %s, specialty = %s, phone = %s
                        WHERE id = %s
                        """,
                        [provider.name, provider.email, provider.specialty,
                         provider.phone, id],
                    )
                    old_data = provider.dict()
                    return PutProviderOut(id=id, **old_data)
        except Exception as T:
            print("T", T)
            return {"message": "Update Failed"}

    def delete(self, id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM provider
                        WHERE id = %s
                        """,
                        [id],
                    )
                    return True
        except Exception as T:
            print("Could not delete", T)
            return False
