from fastapi import APIRouter, Depends, Response
from typing import Union, List, Optional
from queries.appointments import (
                                AppointmentOut,
                                FullAppointmentOut,
                                AppointmentIn,
                                AppointmentRepository,
                                Error
                                )


router = APIRouter()


@router.post("/appointment", response_model=Union[Error, AppointmentOut])
def create_appointment(
    appointment: AppointmentIn,
    response: Response,
    repo: AppointmentRepository = Depends()
):
    response.status_code = 200
    return repo.create(appointment)


@router.get("/appointment", response_model=Union[Error, List[AppointmentOut]])
def get_all(repo: AppointmentRepository = Depends()):
    return repo.get_all()


@router.get("/appointment/{uid}", response_model=Optional[AppointmentOut])
def get_one(
    uid: str,
    response: Response,
    repo: AppointmentRepository = Depends()
) -> AppointmentOut:
    appointment = repo.get_one(uid)
    if appointment is None:
        response.status_code = 404
    return appointment


@router.put("/appointment/{uid}", response_model=Union[Error, AppointmentOut])
def update(
    uid: str,
    appointment: AppointmentIn,
    repo: AppointmentRepository = Depends()
) -> Union[Error, AppointmentOut]:
    return repo.update(uid, appointment)


@router.delete("/appointment/{id}", response_model=bool)
def delete(
    id: int,
    repo: AppointmentRepository = Depends()
) -> bool:
    return repo.delete(id)


@router.get(
        "/appointment/clinic/{c_uid}",
        response_model=Union[Error, List[FullAppointmentOut]]
        )
def get_by_clinic(
    c_uid: str,
    repo: AppointmentRepository = Depends()
):
    return repo.get_by_clinic(c_uid)


@router.get(
        "/appointment/patient/{pat_uid}",
        response_model=Union[Error, List[AppointmentOut]]
        )
def get_by_patient(
    p_uid: str,
    repo: AppointmentRepository = Depends()
):
    return repo.get_by_patient(p_uid)
