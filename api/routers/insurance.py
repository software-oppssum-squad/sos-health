from fastapi import APIRouter, Depends, Response
from typing import Union, List, Optional
from queries.insurance import (
    InsuranceIn,
    InsuranceOut,
    InsuranceRepository,
    Error,
    PutInsuranceOut,
    PutInsuranceIn,
)


router = APIRouter()


@router.post("/insurance", response_model=InsuranceOut)
def create_insurance(
    insurance: InsuranceIn,
    response: Response,
    repo: InsuranceRepository = Depends(),
):
    print(insurance)
    return repo.create(insurance)


@router.get("/insurance", response_model=List[InsuranceOut])
def get_all(repo: InsuranceRepository = Depends()):
    return repo.get_all()


@router.delete("/insurance/{id}", response_model=bool)
def delete_insurance(
    id: int,
    repo: InsuranceRepository = Depends(),
) -> bool:
    return repo.delete(id)


@router.get("/insurance/{id}", response_model=Optional[InsuranceOut])
def get_one(
    id: int,
    response: Response,
    repo: InsuranceRepository = Depends(),
) -> InsuranceOut:
    insurance = repo.get_one(id)
    if insurance is None:
        response.status_code = 404
    return insurance


@router.put("/insurance/{id}", response_model=Union[Error, PutInsuranceOut])
def update_insurance(
    id: int, insurance: PutInsuranceIn, repo: InsuranceRepository = Depends()
) -> Union[Error, PutInsuranceOut]:
    return repo.update(id, insurance)
