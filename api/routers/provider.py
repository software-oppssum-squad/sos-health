from fastapi import APIRouter, Depends, Response
from typing import Union, List, Optional
from queries.provider import (
    ProviderIn,
    ProviderOut,
    ProviderRepository,
    Error,
    PutProviderIn,
    PutProviderOut,
)


router = APIRouter()


@router.get("/provider/{uid}", response_model=Optional[ProviderOut])
def get_one(
    uid: str,
    response: Response,
    repo: ProviderRepository = Depends(),
) -> ProviderOut:
    provider = repo.get_one(uid)
    if provider is None:
        response.status_code = 404
    return provider


@router.get("/provider", response_model=List[ProviderOut])
def get_all(repo: ProviderRepository = Depends()):
    return repo.get_all()


@router.post("/provider", response_model=ProviderOut)
def create_provider(
    provider: ProviderIn,
    response: Response,
    repo: ProviderRepository = Depends(),
):
    print(provider)
    return repo.create(provider)


@router.put("/provider/{id}", response_model=Union[Error, PutProviderOut])
def update_provider(
    id: int, provider: PutProviderIn, repo: ProviderRepository = Depends()
) -> Union[Error, PutProviderOut]:
    return repo.update(id, provider)


@router.delete("/provider/{id}", response_model=bool)
def delete_provider(
    id: int,
    repo: ProviderRepository = Depends()
) -> bool:
    return repo.delete(id)
