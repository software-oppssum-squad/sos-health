from fastapi import (APIRouter, Depends,
                     Response, Request, HTTPException, status)
from pydantic import BaseModel
from typing import List, Optional, Union
from queries.clinic import (
    ClinicIn,
    ClinicOut,
    ClinicRepository,
    PutClinicIn,
    PutClinicOut,
    DuplicateClinicError,
    PrivateAccount,
    PutPasswordIn,
    PutPasswordOut,
    Error
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator

router = APIRouter()


class ClinicForm(BaseModel):
    username: str
    password: str


class ClinicToken(Token):
    clinic: ClinicOut


class HttpError(BaseModel):
    detail: str


@router.post("/clinic", response_model=ClinicToken | HttpError)
async def create_clinic(
    info: ClinicIn,
    request: Request,
    response: Response,
    clinics: ClinicRepository = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        clinic = clinics.create(info, hashed_password)
        print("Successfully created:", clinic)
    except DuplicateClinicError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create a clinic with those credentials",
        )
    form = ClinicForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, clinics)
    print("!!!!!!!!TOKEN!", token)
    return ClinicToken(clinic=clinic, **token.dict())


@router.get("/clinic", response_model=List[ClinicOut])
async def get_all(account_data: dict = Depends(
        authenticator.get_current_account_data),
        repo: ClinicRepository = Depends()):

    return repo.get_all()


@router.delete("/clinic/{id}", response_model=bool)
async def delete_clinic(id: int, account_data: dict = Depends(
        authenticator.get_current_account_data),
        repo: ClinicRepository = Depends(),
) -> bool:
    return repo.delete(id)


@router.get("/clinic/{id}", response_model=Optional[ClinicOut])
async def get_one(
    id: int, response: Response,
    account_data: dict = Depends(
        authenticator.get_current_account_data),
    repo: ClinicRepository = Depends(),
) -> ClinicOut:
    clinic = repo.get_one(id)
    if clinic is None:
        response.status_code = 404
    return clinic


@router.put("/clinic/{id}",
            response_model=Union[Error, PutClinicOut])
async def update_clinic(
    id: int, clinic: PutClinicIn,
    account_data: dict = Depends(
        authenticator.get_current_account_data),
    repo: ClinicRepository = Depends()
) -> Union[Error, PutClinicOut]:
    return repo.update(id, clinic)


@router.get("/clinictoken", response_model=ClinicToken | None)
async def get_token(
     request: Request,
     clinic: PrivateAccount = Depends(
         authenticator.try_get_current_account_data
         )
) -> ClinicToken | None:
    if clinic and authenticator.cookie_name in request.cookies:
        return {
               "access_token": request.cookies[authenticator.cookie_name],
               "type": "Bearer",
               "clinic": clinic
          }


@router.get("/token", response_model=ClinicToken | None)
async def get_ttoken(
     request: Request,
     clinic: PrivateAccount = Depends(
         authenticator.try_get_current_account_data
         )
) -> ClinicToken | None:
    if clinic and authenticator.cookie_name in request.cookies:
        return {
               "access_token": request.cookies[authenticator.cookie_name],
               "type": "Bearer",
               "clinic": clinic
          }


@router.put("/clinic/password/{id}", response_model=PutPasswordOut)
async def update_password(
    id: int, clinic: PutPasswordIn,
    account_data: dict = Depends(
        authenticator.get_current_account_data),
    repo: ClinicRepository = Depends()
):
    hashed_password = authenticator.hash_password(clinic.password)
    return repo.update_password(id, hashed_password)
