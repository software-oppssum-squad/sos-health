from fastapi import APIRouter, Depends, Response
from typing import Union, List, Optional
from queries.patients import (
    PatientIn,
    PatientOut,
    PatientRepository,
    Error,
    PutPatientOut,
    PutPatientIn,
)

router = APIRouter()


@router.get("/patient", response_model=List[PatientOut])
def get_all(repo: PatientRepository = Depends()):
    return repo.get_all()


@router.get("/patient/{uid}", response_model=Optional[PatientOut])
def get_one(
    uid: str,
    response: Response,
    repo: PatientRepository = Depends(),
) -> PatientOut:
    patient = repo.get_one(uid)
    if patient is None:
        response.status_code = 404
    return patient


@router.post("/patient", response_model=PatientOut)
def create_patient(
    patient: PatientIn,
    response: Response,
    repo: PatientRepository = Depends(),
):
    print(patient)
    return repo.create(patient)


@router.put("/patient/{id}", response_model=Union[Error, PutPatientOut])
def update_patient(
    id: int, patient: PutPatientIn, repo: PatientRepository = Depends()
) -> Union[Error, PutPatientOut]:
    return repo.update(id, patient)


@router.delete("/patient/{id}", response_model=bool)
def delete_patient(
    id: int,
    repo: PatientRepository = Depends(),
) -> bool:
    return repo.delete(id)
